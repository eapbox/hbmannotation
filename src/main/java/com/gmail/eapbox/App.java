package com.gmail.eapbox;

import com.gmail.eapbox.repository.*;
import com.gmail.eapbox.repository.exception.DaoHibernateException;
import com.gmail.eapbox.repository.impl.*;
import com.gmail.eapbox.repository.model.*;
import com.gmail.eapbox.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.util.HashSet;
import java.util.Set;

public class App {
        private static final Logger logger = Logger.getLogger(App.class);

        private static final MeetingHibernateDao meetingHibernateDao = new MeetingHibernateDaoImpl(Meeting.class);
        private static final PhoneHibernateDao phoneHibernateDao = new PhoneHibernateDaoImpl(Phone.class);
        private static final RoomHibernateDao roomHibernateDao = new RoomHibernateDaoImpl(Room.class);
        private static final UserHibernateDao userHibernateDao = new UserHibernateDaoImpl(User.class);
        private static final UserInformationHibernateDao userInformationHibernateDao = new UserInformationHibernateDaoImpl(UserInformation.class);

        public static void main(String[] args) throws DaoHibernateException {
                logger.info("Hello World!");

                /*Add User-----------------------------------------
                User user = new User();
                user.setUsername("User20");
                userHibernateDao.insert(user);
                //------------------------------------------------*/

                //*Delete User--------------------------------------
                //userHibernateDao.delete(2);
                //------------------------------------------------*/

                ///*User - UserInformation (1-1) --------------------
                User user = new User();
                UserInformation userInformation = new UserInformation();

                user.setUsername("User21");
                userInformation.setAddress("Street_21");
                userInformation.setUser(user);
                user.setUserInformation(userInformation);
                Integer idUser = userHibernateDao.insert(user);
                //userHibernateDao.delete(idUser);
                //------------------------------------------------*/

                /*User - Phone (1-M)------------------------------
                User user = new User();

                user.setUsername("User5");
                userHibernateDao.insert(user);

                Phone phone1 = new Phone();
                Phone phone2 = new Phone();

                phone1.setPhoneNumber(155);
                phone1.setUser(user);
                user.getPhones().add(phone1);
                phoneHibernateDao.insert(phone1);

                phone2.setPhoneNumber(255);
                phone2.setUser(user);
                user.getPhones().add(phone2);
                phoneHibernateDao.insert(phone2);
                //*/

                //phoneHibernateDao.delete(3);
                //userHibernateDao.delete(4);
                //------------------------------------------------*/

                /*User - Meeting (M-M)-----------------------------
                User user = new User();
                user.setUsername("user1");

                Meeting meeting1 = new Meeting();
                Meeting meeting2 = new Meeting();
                meeting1.setMeetingName("meeting1");
                meeting2.setMeetingName("meeting2");
                Set<Meeting> meetings = new HashSet<Meeting>();
                meetings.add(meeting1);
                meetings.add(meeting2);

                user.setMeetings(meetings);
                userHibernateDao.insert(user);
                //------------------------------------------------*/

        }
}
