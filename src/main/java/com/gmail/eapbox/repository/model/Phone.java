package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_PHONE")
public class Phone implements Serializable {
        private static final long serialVersionUID = 7290228403560076516L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "F_ID", unique = true, nullable = false, length = 5)
        private Integer id;

        @Column(name = "F_PHONE_NUMBER", unique = true, nullable = false, length = 10)
        private Integer phoneNumber;

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "F_USER_ID", nullable = false)
        private User user;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getPhoneNumber() {
                return phoneNumber;
        }

        public void setPhoneNumber(Integer phoneNumber) {
                this.phoneNumber = phoneNumber;
        }

        public User getUser() {
                return user;
        }

        public void setUser(User user) {
                this.user = user;
        }

        @Override
        public String toString() {
                return "Phone(" +
                        "id=" + id +
                        "; userId=" + user.getId() +
                        "; phoneNumber: " + phoneNumber +")";
        }
}
