package com.gmail.eapbox.repository.model;

import java.io.Serializable;
import java.util.Objects;

public class Room implements Serializable {
        private static final long serialVersionUID = -8704474595565173044L;

        private Integer Id;
        private Integer roomNumber;
        private String roomName;
        private Meeting meeting;

        public Meeting getMeeting() {
                return meeting;
        }

        public void setMeeting(Meeting meeting) {
                this.meeting = meeting;
        }

        public Integer getId() {
                return Id;
        }

        public void setId(Integer Id) {
                this.Id = Id;
        }

        public Integer getRoomNumber() {
                return roomNumber;
        }

        public void setRoomNumber(Integer roomNumber) {
                this.roomNumber = roomNumber;
        }

        public String getRoomName() {
                return roomName;
        }

        public void setRoomName(String roomName) {
                this.roomName = roomName;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Room room = (Room) o;
                return Objects.equals(Id, room.Id) &&
                        Objects.equals(roomNumber, room.roomNumber) &&
                        Objects.equals(roomName, room.roomName);
        }

        @Override
        public int hashCode() {
                return Objects.hash(Id,roomNumber, roomName);
        }

        @Override
        public String toString() {
                return "Room(" +
                        "roomId=" + Id +
                         "; roomNumber=" + roomNumber +
                        "; roomName: " + roomName +")";
        }
}
