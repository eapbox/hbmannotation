package com.gmail.eapbox.repository.model;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import org.hibernate.annotations.Parameter;
import java.io.Serializable;

@Entity
@Table(name = "T_USER_INFORMATION")
public class UserInformation implements Serializable {
        private static final long serialVersionUID = 8029467976241746772L;

        @Id
        @GenericGenerator(
                name = "gen",
                strategy = "foreign",
                parameters = @Parameter(name = "property", value = "user")
        )
        @GeneratedValue(generator = "gen")
        @Column(name = "F_ID")
        private Integer id;

        @Column(name = "F_ADDRESS",nullable = false, length = 255)
        private String address;

        @OneToOne(fetch = FetchType.LAZY)
        @PrimaryKeyJoinColumn
        private User user;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getAddress() {
                return address;
        }

        public void setAddress(String address) {
                this.address = address;
        }

        public User getUser() {
                return user;
        }

        public void setUser(User user) {
                this.user = user;
        }

        @Override
        public String toString() {
                return "UserInformation(" +
                        "id=" + id +
                        "; address: " + address +")";
        }
}
