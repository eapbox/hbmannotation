package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "T_MEETING")
public class Meeting implements Serializable {
        private static final long serialVersionUID = -7367358265418541094L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "F_ID", unique = true, nullable = false)
        private Integer id;

        @Column(name = "F_MEETING_NAME", nullable = false, length = 50)
        private String meetingName;

        @ManyToMany(fetch = FetchType.LAZY, mappedBy = "meetings")
        private Set<User> users = new HashSet<User>(0);

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getMeetingName() {
                return meetingName;
        }

        public void setMeetingName(String meetingName) {
                this.meetingName = meetingName;
        }

        public Set<User> getUsers() {
                return users;
        }

        public void setUsers(Set<User> users) {
                this.users = users;
        }

        @Override
        public String toString() {
                return "Meeting(" +
                        "id=" + id +
                        "; meetingName:" + meetingName + ")";
        }
}
