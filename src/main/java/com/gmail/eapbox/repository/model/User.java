package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "T_USER")
public class User implements Serializable {
        private static final long serialVersionUID = -3533655789147213150L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "F_ID",unique = true, length = 5)
        private Integer id;

        @Column(name = "F_USER_NAME", nullable = false, length = 50)
        private String username;

        @OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
        private UserInformation userInformation;

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = CascadeType.ALL)
        private Set<Phone> phones = new HashSet<Phone>(0);

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "T_USER_MEETING", joinColumns = {
                @JoinColumn(name = "F_USER_ID", nullable = false, updatable = false) },
                inverseJoinColumns = { @JoinColumn(name = "F_MEETING_ID",
                        nullable = false, updatable = false) })
        private Set<Meeting> meetings = new HashSet<Meeting>(0);

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public UserInformation getUserInformation() {
                return userInformation;
        }

        public void setUserInformation(UserInformation userInformation) {
                this.userInformation = userInformation;
        }

        public Set<Phone> getPhones() {
                return phones;
        }

        public void setPhones(Set<Phone> phones) {
                this.phones = phones;
        }

        public Set<Meeting> getMeetings() {
                return meetings;
        }

        public void setMeetings(Set<Meeting> meetings) {
                this.meetings = meetings;
        }

        @Override
        public String toString() {
                return "User(" +
                        "id=" + id +
                        "; username: " + username +")";
        }
}
