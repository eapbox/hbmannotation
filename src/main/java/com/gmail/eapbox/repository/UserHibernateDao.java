package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.User;

public interface UserHibernateDao extends GenericHibernateDao<User, Integer>{

        User getUserByUsername(String username);
}
