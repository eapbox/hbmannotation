package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.UserHibernateDao;
import com.gmail.eapbox.repository.model.User;

public class UserHibernateDaoImpl
        extends GenericHibernateDaoImpl<User, Integer>
        implements UserHibernateDao {


        public UserHibernateDaoImpl(Class<User> persistentClass) {
                super(persistentClass);
        }

        public User getUserByUsername(String username) {
                return null;
        }
}
