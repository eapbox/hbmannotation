package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.PhoneHibernateDao;
import com.gmail.eapbox.repository.model.Phone;

public class PhoneHibernateDaoImpl
        extends GenericHibernateDaoImpl<Phone, Integer>
        implements PhoneHibernateDao {

        public PhoneHibernateDaoImpl(Class<Phone> persistentClass) {
                super(persistentClass);
        }
}
