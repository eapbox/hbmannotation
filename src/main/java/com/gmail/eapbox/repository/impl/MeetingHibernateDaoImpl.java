package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.MeetingHibernateDao;
import com.gmail.eapbox.repository.model.Meeting;

public class MeetingHibernateDaoImpl
        extends GenericHibernateDaoImpl<Meeting, Integer>
        implements MeetingHibernateDao {

        public MeetingHibernateDaoImpl(Class<Meeting> persistentClass) {
                super(persistentClass);
        }
}
