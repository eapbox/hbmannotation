package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.GenericHibernateDao;
import com.gmail.eapbox.repository.exception.DaoHibernateException;
import com.gmail.eapbox.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.util.List;

public abstract class GenericHibernateDaoImpl<T extends Serializable, ID extends Serializable>
        implements GenericHibernateDao<T, ID> {
        private static final Logger logger = Logger.getLogger(GenericHibernateDaoImpl.class);

        private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        private Class<T> persistentClass;

        public GenericHibernateDaoImpl(Class<T> persistentClass) {
                this.persistentClass = persistentClass;
        }

        public Class<T> getPersistentClass() {
                return persistentClass;
        }

        public ID insert(T entity) throws DaoHibernateException {
                Session session = null;
                ID id;
                try {
                        session = sessionFactory.getCurrentSession();
                        session.beginTransaction();
                        id = (ID) session.save(entity);
                        session.getTransaction().commit();
                } catch (HibernateException e) {
                        logger.error("HibernateException during insert: " + entity);
                        session.getTransaction().rollback();
                        throw new DaoHibernateException(e);
                }
                return id;
        }

        public void update(T entity) throws DaoHibernateException {
                Session session = null;
                try {
                        session = sessionFactory.getCurrentSession();
                        session.beginTransaction();
                        session.update(entity);
                        session.getTransaction().commit();
                } catch (HibernateException e) {
                        logger.error("HibernateException during insert: " + entity);
                        session.getTransaction().rollback();
                        throw new DaoHibernateException(e);
                }
        }

        public void delete(ID id) throws DaoHibernateException {
                Session session = null;
                T entity;
                try {
                        session = sessionFactory.getCurrentSession();
                        session.beginTransaction();
                        entity = (T) session.get(getPersistentClass(), id);
                        session.delete(entity);
                        session.getTransaction().commit();
                } catch (HibernateException e) {
                        logger.error("HibernateException during insert: " + id);
                        session.getTransaction().rollback();
                        throw new DaoHibernateException(e);
                }
        }

        public T findById(ID id) throws DaoHibernateException {
                Session session = null;
                T entity;
                try {
                        session = sessionFactory.getCurrentSession();
                        session.beginTransaction();
                        entity = (T) session.get(getPersistentClass(), id);
                        session.getTransaction().commit();
                } catch (HibernateException e) {
                        logger.error("HibernateException during findById: " + id);
                        session.getTransaction().rollback();
                        throw new DaoHibernateException(e);
                }
                return entity;
        }

        public List<T> findAll() throws DaoHibernateException {
                Session session = null;
                List<T> list = null;
                try {
                        session = sessionFactory.getCurrentSession();
                        session.beginTransaction();
                        list = session.createQuery("from " + getPersistentClass().getName()).list();
                } catch (HibernateException e) {
                        logger.error("HibernateException during findAll: ");
                        throw new DaoHibernateException(e);
                }
                return list;
        }
}
