package com.gmail.eapbox.repository.util;

import com.gmail.eapbox.repository.model.Meeting;
import com.gmail.eapbox.repository.model.Phone;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserInformation;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
        private static final Logger logger = Logger.getLogger(HibernateUtil.class);
        private static final SessionFactory sessionFactory = buildSessionFactory();

        private static SessionFactory buildSessionFactory() {
                try {

                        Configuration configuration = new Configuration();
                        configuration.addAnnotatedClass(User.class);
                        configuration.addAnnotatedClass(UserInformation.class);
                        configuration.addAnnotatedClass(Phone.class);
                        configuration.addAnnotatedClass(Meeting.class);
                        return configuration.configure().buildSessionFactory();
                }
                catch (Throwable ex) {
                        // Make sure you log the exception, as it might be swallowed
                        logger.error("Initial SessionFactory creation failed." + ex);
                        throw new ExceptionInInitializerError(ex);
                }
        }

        public static SessionFactory getSessionFactory() {
                return sessionFactory;
        }
}
