package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.Phone;

public interface PhoneHibernateDao extends GenericHibernateDao<Phone, Integer>{
}
